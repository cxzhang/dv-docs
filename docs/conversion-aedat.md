---
id: conversion-aedat
title: How To Convert old AEDAT2/3 files to AEDAT4
sidebar_label: Convert old AEDAT2/3 files
categories: []
---

During the years we progressively updated the AEDAT file format for our output files.
The latest version is number 4 and it is the version currently used by the DV SDK.
To continue using older AEDAT2 and AEDAT3 files, please convert them to the AEDAT4 format.

Converting old formats using DV is pretty easy and straightforward.
Just follow the steps below:

- Go to `Maintenance` (top menubar in DV) and select `Convert files to aedat4`

![maintenance](assets/convert-maintenance.png)

- Add the files you want to convert

![add files](assets/convert-add.png)

- Select the output directory and then press `Start`

![start](assets/convert-start.png)

- After you press `Start` all the files added will be converted to AEDAT4.
When the conversion is finished you should see all files with a green check mark in the status column.
In case the conversion fails for any reason, a yellow symbol will be displayed, and you will be able
to get more details on what went wrong.

![convert-done](assets/converter-done.png)

Now you will find all the converted files in the output directory.
During the conversion the converter will keep the filename, but will change the file extension to AEDAT4.

You can now use the standard `File Input` module and functionality with your newly converted files.

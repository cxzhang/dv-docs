---
id: additional-modules
title: More Modules
sidebar_label: More Modules
---

DV comes with a pre-installed set of modules. In addition to that, we provide additional Modules to extend the functionality of DV.

## Stereo SLAM
A Stereo SLAM Module with two synchronized DVXplorer cameras.

<a class="button" href="https://gitlab.com/inivation/dv/dv-stereo-slam">
    DV-STEREO-SLAM
</a>




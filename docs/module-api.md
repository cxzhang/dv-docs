---
id: module-api
title: Module API
sidebar_label: Module API
---

# Overview

A DV module is processing unit that takes some streams as inputs, does operations on them, and outputs streams as outputs.
Typically, a module takes on polarity event data, does computer vision algorithms on the data and outputs the results.
Input and outputs can have arbitrary types.

## Module Basics

Every DV module inherits from the `dv::ModuleBase` class. In order to register a module class (that inherits from `dv::ModuleBase`) as a DV module, the following macro has to be called at the end of the file where the class is defined:

```C++
registerModuleClass(RefractoryPeriodFilter)
```

### Fields

Inheriting from the this class gives access to the following fields

Field       | Type                   | Purpose
------------|------------------------|---------------------------------------------------------------
`config`    | dv::RuntimeConfig      | Access config values from config options.
`log`       | dv::Logger             | A configured logging instance to log values to DV
`inputs`    | dv::RuntimeInputs      | Access to the inputs of the module to get data
`outputs`   | dv::RuntimeOutputs     | Access to the outputs of the module to send data




### Static Methods

Every module has to provide some static methods to work properly with DV. The static methods must have the exact footprint as described in the list below. **You have to provide these methods.**

***

```C++
static const char *initDescription()
```
Return a description of what the module does
**This function is required**

***

```C++
static void initInputs(dv::InputDefinitionList &in)
```
Define the inputs to the module

***

```C++
static void initOutputs(dv::OutputDefinitionList &out)
```
Define the outputs to the module

***

```C++
static void initConfigOptions(dv::RuntimeConfig &config)
```
Define configuration options for the module

***

### Overridable Methods

`dv::BaseModule` exhibits two overridable functions to implement the functionality of the module.

***

```C++
void run()
```
Gets called periodically or as new data arrives. Should do the processing. **This function is required**

***

```C++
void configUpdate()
```
Gets called whenever the user changes the configuration. Used to read new configuration values and store them in instance data members.

***

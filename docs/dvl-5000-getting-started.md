---
id: dvl-5000-getting-started
title: Getting Started
sidebar_label: Getting Started
---

## General Overview
The components of the DVL-5000 can be divided into three parts:

- The sensor itself, consisting of the DVXplorer, the laser, and the laser mounting parts
- A host computer, which interfaces the Raspberry Pi using the DV-GUI
- The Raspberry Pi, on which the DVL-5000 software runs

## Preparing the Sensor
The sensor can be assembled very easily using the assembly instructions delivered with the DVL-5000 components.

## Preparing the Host Computer
In order to prepare the host computer, please refer to [the general getting started guide](./getting-started.md). In order to use the DVL-5000, only the DV-GUI is needed, however, in order to develop your own modules based on the DVL-5000, or for DV algorithms in general, it is suggested to install the DV-SDK as well.

## Connecting to the Raspberry Pi
### Setting up a TCP Connection Between the Raspberry Pi and the Host Computer
The Raspberry Pi comes preinstalled with all necessary components. Whenever the Raspberry Pi is booted, the DVL-5000 software is automatically started as a Linux service. To connect to the Raspberry Pi, please refer to the [official documentation](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up/3). In order to use the DVL-5000, you will need to connect to the Raspberry Pi either via WiFi or Ethernet.

If a DNS is available, the Raspberry Pi can be reached via the hostname dvl-5000.

### Connecting DV to the Raspberry Pi
Once the Raspberry Pi is powered up and reachable, a connection to DV can be established as follows:

- Open DV
- Select `Connect to` &rarr; `Connect to remote runtime`
- Enter the IP address or hostname of the Raspberry Pi
- For the port number, enter 4040

### Obtaining the Sample Configurations
The sample configurations for the DVL-5000 are available [here as a .tar.gz archive](http://release.inivation.com/dvl-5000/dv-config.tar.gz) and [here as a .zip archive](http://release.inivation.com/dvl-5000/dv-config.zip). They should be stored on the host computer, and can be opened in DV by selecting `File` &rarr; `Open project`.

### Obtaining Software Updates
Software updates can be obtained by logging in to the Raspberry Pi, and running the bash script available from [this link](http://release.inivation.com/dvl-5000/dvl-5000-setup.sh). The username on the Raspberry Pi is `ubuntu`, and the password is `dvl-5000`.

```bash
ssh ubuntu@dvl-5000.local
wget http://release.inivation.com/dvl-5000/dvl-5000-setup.sh
sudo bash ./dvl-5000-setup.sh
```

If there is no DNS available, instead of `dvl-5000`, you need to specify the IP address.

The available options are as follows:

```bash
Usage:
    dvl-5000-setup.sh -h | --help                    Display this help message.
    dvl-5000-setup.sh                                Install the latest version of all components.
    dvl-5000-setup.sh -<component>=<version>         Install a specific version of a certain component.

Components:
    -p, --dv-laser-profiler                          dv-laser-profiler
    -c, --dv-laser-profiler-calibration              dv-laser-profiler-calibration
    -r, --dv-point-cloud-renderer                    dv-point-cloud-renderer
    -h, --dv-point-cloud-running-heat-map            dv-point-cloud-running-heat-map
    -o, --dv-point-cloud-csv-output                  dv-point-cloud-csv-output
    -l, --dv-latency-measurement                     dv-latency-measurement

Versions:
    MAJOR.MINOR.PATCH                                specific version
    latest                                           latest release
    master                                           latest master branch (unstable track)
```

---
id: getting-started
title: Get Started
sidebar_label: Get started
categories: []
---

![DV software screenshot](assets/screen.png)

**DV** is the software for the [iniVation **Dynamic Vision Sensors (DVS/DAVIS)**](https://inivation.com/buy). DV connects to your camera and shows its output. It is also the official Software Development Kit to write application software for event-based cameras.

# Download

## Windows

<a class="button" href="http://release.inivation.com/gui/latest-win-stable">
    ![Windows](assets/icons/windows.svg) Download for Windows (64-bit)
</a>



## MacOS

<a class="button" href="http://release.inivation.com/gui/latest-mac-stable">
    ![macOS](assets/icons/apple.svg) Download for macOS (64-bit)
</a>


#### Development runtime

If you plan to develop your own modules on a mac, the development runtime is required. The easiest way to install it is via [Homebrew](https://brew.sh/).
We provide a [Homebrew tap](https://gitlab.com/inivation/homebrew-inivation/) for macOS. Install the development runtime with:

```bash
brew tap inivation/inivation
brew install libcaer --with-libserialport --with-opencv
brew install dv-runtime
```
You can also pass `--HEAD` to build from the latest Git master automatically.

If you have a mac with Apple Silicon processor, the development runtime runs natively on arm64.



## Ubuntu Linux

We provide a [PPA repository](https://launchpad.net/~inivation-ppa/+archive/ubuntu/inivation)
for Ubuntu Bionic (18.04 LTS), Focal (20.04 LTS) and Groovy (20.10)
on the x86_64, arm64, armhf and ppc64 architectures.
Please note that dv-gui is only available on x86_64, the dv-runtime on all supported architectures.

Please note that on 32-bit ARM (armhf) only, we do not currently support the clang compiler.
Further, you might have to start dv-runtime with the following environment variable set: `UNW_ARM_UNWIND_METHOD=4`.

```bash
sudo add-apt-repository ppa:inivation-ppa/inivation
sudo apt-get update
sudo apt-get install dv-gui
```

If you plan to develop your own modules, please install the following additional package:

```bash
sudo apt-get install dv-runtime-dev
```

For Ubuntu Xenial (16.04 LTS), we offer a [separate PPA repository](https://launchpad.net/~inivation-ppa/+archive/ubuntu/inivation-xenial)
supporting the x86_64 and x86 architectures.
Please note this will be retired in April 2021 when official Xenial LTS support will end!
Please note that you will have to add several additional PPA repositories to get the versions
of the dependencies we require:

```bash
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo add-apt-repository ppa:lkoppel/opencv
sudo add-apt-repository ppa:janisozaur/cmake-update
sudo add-apt-repository ppa:inivation-ppa/inivation-xenial
sudo apt-get update
sudo apt-get install dv-gui
```


## Arch Linux

You can find DV in the AUR repository, install the package 'dv-gui', which depends on all the components of the DV software platform.

The standard packages in the AUR repository already include all development files.
We also offer a dv-runtime-git package that builds the latest Git master automatically, in case you need the latest fixes and functionality.


## Fedora Linux

We provide a [COPR repository](https://copr.fedorainfracloud.org/coprs/inivation/inivation/)
for Fedora 32, 33 and rawhide on the x86_64, arm64 and armhf architectures.
Please note that dv-gui is only available on x86_64, the dv-runtime on all supported architectures.

Please note that on 32-bit ARM (armhf) only, we do not currently support the clang compiler.
Further, you might have to start dv-runtime with the following environment variable set: `UNW_ARM_UNWIND_METHOD=4`.

```bash
sudo dnf copr enable inivation/inivation
sudo dnf install dv-gui
```

If you plan to develop your own modules, please install the following additional package:

```bash
sudo dnf install dv-runtime-devel
```

## Gentoo Linux

A valid Gentoo ebuild repository is available [here](https://gitlab.com/inivation/gentoo-inivation/)
over Git. The package to install is 'dev-util/dv-gui'.

The standard packages in the Gentoo ebuild repository already include all development files.
We also offer a =dev-util/dv-runtime-9999 package that builds the latest Git master automatically, in case you need the latest fixes and functionality.


## Embedded Devices
### Nvidia Jetson

The Nvidia Jetson embedded boards are supported via [our Ubuntu packages](#ubuntu-linux).
The [Linux for Tegra (L4T)](https://developer.nvidia.com/embedded/linux-tegra) distribution is based on Ubuntu 18.04 LTS, so the same installation instructions apply.


### Raspberry Pi

Raspberry Pi is supported on the Ubuntu operating system. Ubuntu is an [officially supported OS for Raspberry Pi](https://ubuntu.com/download/raspberry-pi). Please use [our Ubuntu packages](#ubuntu-linux).

### Launching DV-Runtime with a Remote GUI

For remote, head-less runtime deployments, we support and recommend using
the system service daemons present in all major Linux distributions: SystemD or OpenRC.
Our distribution packages will install the necessary control scripts by default,
users only have to enable them as needed following the below procedure:

1. Edit the startup scripts at */usr/lib/systemd/system/dv-runtime.service* (SystemD) or */etc/init.d/dv-runtime* (OpenRC).
Change the IP address from `127.0.0.1` (listen on local host only) to `0.0.0.0` (listen on all network interfaces).

2. Start the service using the appropriate command:
    * SystemD: `$ sudo systemctl start dv-runtime.service`
    * OpenRC: `$ sudo /etc/init.d/dv-runtime start`

3. Optional: set the service to automatically start on boot:
    * SystemD: `$ sudo systemctl enable dv-runtime.service`
    * OpenRC: `$ sudo rc-update add dv-runtime default`

When the application is running change the configuration with dv-control or with a remote connection with the GUI (recommended for easy set up).
In the GUI, go to 'Connect to' in the top menu and select 'Connect to remote runtime ...', enter the IP address of the system running the runtime
and the port on which the runtime is listening (default is 4040). Press 'Connect' and you're done!

If the connection does not work, or visualizers fail to show data, check that there are no firewalls or anything else blocking TCP connections.
TCP port 4040 is needed for configuration exchange, plus one random TCP port in the [ephemeral range](https://en.wikipedia.org/wiki/Ephemeral_port) for each visualizer to transmit data to show.

## Bleeding edge releases

We provide a bleeding edge release with the latest features directly from development. 
**Bleeding edge releases are untested and are potentially broken.** Use at your own risk or when directed to do.

* [Latest Git master Windows build (64-bit)](http://release.inivation.com/gui/latest-win-master)
* [Latest Git master macOS build (64-bit)](http://release.inivation.com/gui/latest-mac-master)


---
id: dvl-5000-calibration
title: Calibration
sidebar_label: Calibration
---

## Camera Calibration
For the calibration of the profiler to be conducted, the camera itself needs to be calibrated.

### Preparation:
1. Open DV.
1. Select `Connect to` &rarr; `Remote runtime` and enter the IP address of the DVL-5000. The port is `4040`.
    1. If the DVL-5000 is connected to a DNS, you can enter `dvl-5000` instead of the IP address.
1. Open the DV configurations downloaded from the release site: [.tar.gz](http://release.inivation.com/dvl-5000/dv-config.tar.gz) or [.zip](http://release.inivation.com/dvl-5000/dv-config.zip).
    1. Select `File` &rarr; `Open project` and select the path to the folder to which you extracted the downloaded archive containing the DV configurations.
    1. Select the DV configuration named `dvl-5000-camera-calibration.xml`.
1. Plug in the DVL-5000.
1. Mount the DVL-5000 to the place of its intended use.
1. Print out the [Siemens Star](http://release.inivation.com/dvl-5000/siemensStar.svg) and the [camera calibration pattern](http://release.inivation.com/dvl-5000/cameraCalibrationPattern_200x150_6x9_10_3.pdf).
1. Start all of the modules, and switch to the `Output` tab in DV.
1. Focus the lens using the printed Siemens Star.
    1. Open up the aperture completely using the lens' ring that says `open` and `close`.
    1. Adjust the field of view to cover the desired measurement range using the lens' ring that says `near` and `far`.
    1. Focus of view using the lens' ring that says `tele` and `wide`. When the lens is perfectly in focus, the centre of the Siemens Star is visible without any shade.
1. Lock all of the rings on the lens using the screws in them.

![Siemens Star](assets/siemensStarFocusing.png)

### Execution:
1. Move around the camera calibration pattern in front of the DVL-5000.
1. Once the calibration pattern is detected, the area in which it was detected will gradually be covered in a green overlay. Try to cover as much of the frame in green as possible.
1. Once enough calibration points have been collected, they will be displayed again for review. They can be accepted or rejected by clicking `keep` or `discard`, respectively.
1. After the collected images have been reviewed, the calibration is calculated, and the results are displayed in the runtime log.
1. If the reprojection error is in an acceptable range, the calibration results are stored in `/home/ubuntu/calibration_camera.xml`.

For more in-depth information on the camera calibration, please refer to [the camera calibration tutorial](./tutorial-calibration.md).

![Asymmetric Circles Calibration](assets/asymmetric-circles-calibration.png)


## Profiler Calibration
After the camera is calibrated, the profiler can be calibrated.

1. As described above, the DVL-5000 should already be mounted to the place of its intended use.
1. Place the calibration object in front of the DVL-5000.
    1. The laser line should be visible on all levels of the calibration piece.
    1. The pictures below show how the DVL-5000 and the calibration object should be positioned.
    1. Make sure the laser is focused for its intended measurement range.
1. Open DV.
1. Select `Connect to` &rarr; `Remote runtime` and enter the IP address of the DVL-5000. The port is `4040`.
    1. If the DVL-5000 is connected to a DNS, you can enter `dvl-5000` instead of the IP address.
1. Open the DV configurations downloaded from the release site: [.tar.gz](http://release.inivation.com/dvl-5000/dv-config.tar.gz) or [.zip](http://release.inivation.com/dvl-5000/dv-config.zip).
    1. Select `File` &rarr; `Open project` and select the path to the folder to which you extracted the downloaded archive containing the DV configurations.
    1. Select the DV configuration named `dvl-5000-profiler-calibration.xml`.
1. Start all of the modules, and switch to the `Output` tab in DV.
1. Click "Collect" in order to collect calibration data.
    1. The "Collect" button is highlighted in the figure below.
1. Click "Calibrate" in order to conduct the calibration.
    1. The "Calibrate" button is highlighted in the figure below.

The figures below show an example physical setup for the calibration and the resulting output in DV.

![DVL-5000 Calibration Setup](assets/dvl-5000-calibration-setup.jpg)
![DVL-5000 Calibration Visualization](assets/dvl-5000-calibration-screenshot-visualization.png)

The figure below shows the highlighted buttons for the calibration and testing.
![DVL-5000 Calibration Configuration Buttons](assets/dvl-5000-profiler-calibration-config-buttons.png)

The DVL-5000 should be running automatically. If it is not running, enable the DVL-5000 by checking the box "Enable signal generator" under the external input configuration.

![DVL-5000 Enable](assets/dvl-5000-external-input.png)

## Testing the Calibration Results
The calibration results can be tested with an object of constant height that covers the whole DV sensor. It is also possible to test with the DVL-5000 pointed on the ground plane (i.e. the test height being 0).

The test is conducted as follows:
1. Point the DVL-5000 at the ground plane.
1. Open DV.
1. Select `Connect to` &rarr; `Remote runtime` and enter the IP address of the DVL-5000. The port is `4040`.
    1. If the DVL-5000 is connected to a DNS, you can enter `dvl-5000` instead of the IP address.
1. Open the DV configurations downloaded from the release site: [.tar.gz](http://release.inivation.com/dvl-5000/dv-config.tar.gz) or [.zip](http://release.inivation.com/dvl-5000/dv-config.zip).
    1. Select `File` &rarr; `Open project` and select the path to the folder to which you extracted the downloaded archive containing the DV configurations.
    1. Select the DV configuration named `dvl-5000-profiler-calibration.xml`.
1. Start all of the modules, and switch to the `Output` tab in DV.
1. Click "Collect" in order to collect calibration data.
    1. The "Collect" button is highlighted in the figure below.
1. Click "Test" in order to conduct the test.
    1. The "Test" button is highlighted in the figure below.


The figure below shows the highlighted buttons for the calibration and testing.
![DVL-5000 Calibration Configuration Buttons](assets/dvl-5000-profiler-calibration-config-buttons.png)

Once the test is finished, a visualization of the error distribution of will be displayed, as shown below. The collected data are colour coded based on their measurement error, with a colourbar displayed on the right.

![DVL-5000 Calibration Results](assets/dvl-5000-test-results.png)

### Advanced Calibration targeted at Specific Object Sizes and Measurement Ranges
While the calibration piece provided is targeted at a very wide measurement range, it may be useful to calibrate for a smaller or larger range. The `dv-profiler-calibration` module allows for any calibration object to be used, provided there are more than three different levels of height, the difference in these levels being constant. For example you use a stair with five steps, as long as the step heights is the same for all steps.

The advanced configuration options for the profiler calibration module are shown in the image below. In order to use a custom calibration object suited specifically for your application, you can modify the step height (in `mm`) and the number of steps.

![DVL-5000 Calibration Advanced Configuration](assets/dvl-5000-profiler-calibration-config-advanced.png)

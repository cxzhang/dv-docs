---
id: dvl-5000-scanning
title: Scanning
sidebar_label: Scanning
---

## Module Structure
A sample configuration for running, visualizing, recording, and exporting scans with the DVL-5000 is available on the [iniVation release server](https://s3.eu-central-1.amazonaws.com/release.inivation.com/dvl-5000/dv-config.tar.gz). A screenshot of the structure is shown below.

![DVL-5000 Scanning Structure](assets/dvl-5000-profiler-scanning-modules.png)

## Module Descriptions

| Module | Description |
|--------|-------------|
| DV Undistort | Undistortion of captured events. |
| DVL Laser Profiler | Extraction of laser line from events and mapping of extracted laser line into 3D space. |
| DVL Point Cloud Renderer | Rendering of extracted profiles on a 2D graph. |
| DVL Running Heat Map | Rendering of extracted profiles in a running heat map. |
| DVL Latency Measurement | Measuring latency between captured events and computed point cloud. |
| DVL Point Cloud CSV Output | CSV export of point cloud data. |

## Module Configuration
| Module | Configuration |
|--------|-------------|
| DV Undistort | Set the camera calibration path. |
| DVL Laser Profiler | The output unit can be chosen between `physical` units (in `mm`) and `pixel`. If `physical` is selected, the profiler calibration path needs to be set.  |
| DVL Point Cloud Renderer | No configuration necessary. |
| DVL Running Heat Map | Select the data range which should be displayed (in the same units as selected for the profiler). A subsampling rate may be chosen if needed. |
| DVL Latency Measurement | No configuration necessary. |
| DVL Point Cloud CSV Output | Set the path to the output file. |

### Running the DVL-5000
1. Open DV.
1. Select `Connect to` &rarr; `Remote runtime` and enter the IP address of the DVL-5000. The port is `4040`.
    1. If the DVL-5000 is connected to a DNS, you can enter `dvl-5000` instead of the IP address.
1. Open the DV configurations downloaded from the release site: [.tar.gz](http://release.inivation.com/dvl-5000/dv-config.tar.gz) or [.zip](http://release.inivation.com/dvl-5000/dv-config.zip).
    1. Select `File` &rarr; `Open project` and select the path to the folder to which you extracted the downloaded archive containing the DV configurations.
    1. Select the DV configuration named `dvl-5000-camera-capture.xml`.
1. In the profiler's calibration options, select between `pixel` and `physical` for the output type.
1. Start the desired modules, and switch to the `Output` tab in DV.

![DVL-5000 Enable](assets/dvl-5000-profiler-config.png)

The DVL-5000 should be running now. If it is not running, enable the DVL-5000 by checking the box "Enable signal generator" under the external input configuration.

![DVL-5000 Enable](assets/dvl-5000-external-input.png)

The figure below shows a sample output of the DVL Running Heat Map module.

![DVL-5000 Coin Scan](assets/dvl-5000-coin-scan.png)

## Notes for Obtaining the Best Possible Results
- Always undistort the events using the dv-undistort module.
- Focus the lens using a [Siemens Star](https://en.wikipedia.org/wiki/Siemens_star).
  - Try to get a good depth of focus by reducing the aperture, but make sure that the laser line is still visible.
- Focus the laser to the region of interest.
- The bias settings in the DVXplorer module have an impact on the quality of the extracted laser line.
  - If the object to be scanned is very close to the DVL-5000, lower bias settings tend to yield better results.
  - If the object to be scanned is farther away from the DVL-5000, higher bias settings tend to yield better results.
- If possible, ensure that the measurement range covers the entire image range.

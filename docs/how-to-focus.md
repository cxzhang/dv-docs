---
id: focus
title: How To Focus an Event Camera
sidebar_label: Focus an Event Camera
categories: []
---

Focusing can be tricky with any camera, but if you don't have normal frames it will be even harder.
In this small tutorial you will learn how to properly focus an event-only camera.

The easiest way to focus an event-only camera is using a Siemens Star [(PDF download)](assets/siemens_star_24.pdf).
This particular pattern allows focusing the camera in a minute.

![Siemens Star](assets/siemens_star.png)

To focus the camera, you have to follow these steps:

- Print the Siemens Star [(PDF download)](assets/siemens_star_24.pdf) and go near the object you want to focus,
place it in a way that will be visible from the camera.

- Start the DV software and connect the event output from the camera directly to the visualizer.

![structure](assets/focus_structure.png)

- Then enlarge the output visualizer or make it fullscreen by clicking the dedicated button.

![visualizer](assets/focus_visualizer.png)

- Now you can start to rotate the focus ring on the lens. Keep generating events by slightly jiggling
either the camera or the pattern. At first you will probably see something like this:

![not in focus](assets/focus_notfocus.png)

- You will be in focus when the center of the pattern is visible and sharp:

![in focus](assets/focus_focus.png)

Now that you have the camera focused properly, you can run your application without problems.

*Remember that for a bigger depth of field (focus range) you need to have a higher f-number,
but this will also reduce the amount of light that the sensor will receive.*

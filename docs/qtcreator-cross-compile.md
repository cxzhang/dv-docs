---
id: qtcreator-crosscompilation
title: QtCreator - Raspberry Pi Cross Compilation
sidebar_label: Cross Compilation
---

Developing Dynamic Vision modules for embedded systems and remote deployment has never been so *much fun*.
Follow these steps  to easily set-up the toolchain for cross-compilation and deployment with your *favorite* IDE QtCreator. We will be using **QtCreator 4.9** on **Ubuntu 19.04** and a **Raspberry Pi 3** running **Raspbian Buster**.

### Prerequisites

1.  [**Download**](https://www.qt.io/download) QtCreator 4.9.

2.  [**Install**](getting-started.md#raspbian-buster) dv-runtime-dev on the Raspberry Pi. SSH needs to be enabled (see [here](https://www.raspberrypi.org/documentation/remote-access/ssh/)).
    On your computer, **open the file** `/etc/ssh/ssh_config` and **comment out the line** `SendEnv LANG LC_*`.

3.  **Create a module** you want to deploy to the Raspberry Pi, e.g. the [example module](first-module.md#Clonethestartmodulerepositorywithgit).

### Set-up Cross Compilation Toolchain

1.  **Install the C and C++ cross compilers**:
    ```
    sudo apt-get install gcc-8-arm-linux-gnueabihf g++-8-arm-linux-gnueabihf
    ```

2.  Apart from the C and C++ compilers the /usr and /lib directories from the Raspberry Pi are required, as well as a cmake toolchain file. **Create a file for the Pi root file system**, e.g.:
    ```
    mkdir ~/pi/
    ```
    **Copy the /usr and /lib directories** from the Pi into it. Rsync can be used for that (**replace [ADDRESS]** with the correct IP address):
    ```
    rsync -rl --delete-after --safe-links pi@[ADDRESS]:/{lib,usr} ~/pi/
    ```
3.  **Create a cmake toolchain** file:
    ```
    touch ~/pi/pi.cmake
    ```
    and **copy the following** into it:
    ```CMake
    set( TC_PATH "/usr/bin" )
    set( ROOTFS_PATH "~/pi" )
    set( CROSS_LOCAL_PREFIX "/usr" )

    set( CMAKE_SYSTEM_NAME Linux )
    set( CMAKE_SYSTEM_PROCESSOR arm )

    set( CMAKE_C_COMPILER "${TC_PATH}/arm-linux-gnueabihf-gcc-8" )
    set( CMAKE_CXX_COMPILER "${TC_PATH}/arm-linux-gnueabihf-g++-8" )
    set( CMAKE_LINKER "${TC_PATH}/arm-linux-gnueabihf-ld" )
    set( CMAKE_AR "${TC_PATH}/arm-linux-gnueabihf-ar" )
    set( CMAKE_OBJCOPY "${TC_PATH}/arm-linux-gnueabihf-objcopy" )

    set( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER )
    set( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY )
    set( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY )
    set( CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY )

    set( CMAKE_PREFIX_PATH ${ROOTFS_PATH} )
    set( CMAKE_INSTALL_PREFIX "${ROOTFS_PATH}${CROSS_LOCAL_PREFIX}" )
    set( CMAKE_FIND_ROOT_PATH ${ROOTFS_PATH} )
    set( ENV{PKG_CONFIG_PATH} "${ROOTFS_PATH}${CROSS_LOCAL_PREFIX}/lib/pkgconfig/" )
    set( ENV{PKG_CONFIG_SYSROOT_DIR} ${ROOTFS_PATH} )

    set( USER_LOCAL_PREFIX ${CROSS_LOCAL_PREFIX} )
    ```

### Set-up QtCreator

1.  Connect the Raspberry Pi with QtCreator. Follow the steps from [here](https://doc.qt.io/qtcreator/creator-developing-generic-linux.html) to **add it as an Embedded Linux Device**. The final result under **Tools** &rarr; **Options** &rarr; **Devices** should look similar to this:

    ![Screenshot, adding a Raspberry Pi as a device](assets/docs_raspi_linuxdevice.png)

2. Add the cross compilers to Qt. Go to **Tools** &rarr; **Options** &rarr; **Kits** &rarr; **Compilers** and **add the compilers for C and C++** (arm-linux-gnueabihf-gcc-8 and arm-linux-gnueabihf-g++-8, respectively). The result should look something like this:

    ![Screenshot, adding cross compilers](assets/docs_adding_compilers_qtc.png)

3. Next **add a new kit** under **Tools** &rarr; **Options** &rarr; **Kits**, named e.g. "Raspberry Pi".
    1.  **Select "Generic Linux Device"** as *Device type* and select the previously added **"Raspberry Pi"** as *Device*.
    2.  **Set Sysroot** to the file containing the Pi's `/usr` and `/lib`, e.g. `~/pi/`.
    3.  **Select the just added compilers** for C and C++ and **select None** for *Debugger* and *Qt version*.
    4.  Finally, **change Cmake Configuration** to only contain the following line:
        ```CMake
        CMAKE_TOOLCHAIN_FILE=~/pi/pi.cmake
        ```
    The overall result should look similar to this:

    ![Screenshot, adding the cross compilation kit](assets/docs_kit.png)

4.  Following [this tutorial](https://doc.qt.io/qtcreator/creator-deployment-embedded-linux.html#deploying-cmake-projects-to-embedded-linux-devices), we **add a file named "QtCreatorDeployment.txt" to the module directory** we want to deploy in order to tell QtCreator where to copy the created module library onto the device, e.g. `/home/pi/modules`. You can tell it to copy other files too, but in this case we just need the the module library. An example file could look like this:

    ![Screenshot, adding the cross compilation kit](assets/docs_qtcreatordeploy.png)

    Do not forget to also **add this path** to the dv-runtime config **on the Raspberry Pi**. You can ssh into the Raspberry Pi and (with a running dv-runtime) use dv-control to include the path:
    ```
    OLDPATH=$(dv-control -s get /system/modules/ modulesSearchPath | cut -c 6-)

    dv-control -s put /system/modules/ modulesSearchPath "$OLDPATH|/home/pi/modules"
    ```

5. **Open the module** you want to deploy as a project in QtCreator, e.g. the example module from (link to example module). Select the Raspberry Pi Kit and an according build directory, e.g. `~/pi_example_build/`. Verify that the deploy paths are correct under **Projects** (Sidebar) &rarr; **Build & Run** &rarr; **Raspberry Pi** &rarr; **Run**. The "Deployment" section should look similar to this:

    ![Screenshot, adding the cross compilation kit](assets/docs_deploy.png)

    Additionally, **replace the "Kill current application instance"** with "Run custom remote command" containing the following line:
    ```
    dv-control -s put /system/ running false
    ```
    This will cause the current dv-runtime instance to orderly finish running so you can restart it with the next command.

6. To automatically start the runtime, **change the Run configuration** to "Custom Executable (on Raspberry Pi) with `dv-runtime` as executable and `-i 0.0.0.0` as command line argument (which enables remote GUI access).

    ![Screenshot, adding the cross compilation kit](assets/docs_run_remote.png)

#### Have fun deploying your awesome modules remotely to a Raspberry Pi.

# DV documentation

# [Online DV Documentation is here](http://inivation.gitlab.io/dv/dv-docs)

[Docusaurus](https://docusaurus.io) based documentation for dv.

## Compile the docs yourself:

* Make sure you have recent nodejs installed
* Clone repository
* `cd website`
* `npm install`
* `npm start`

/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');
const classnames = require('classnames');

const MarkdownBlock = CompLibrary.MarkdownBlock; /* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

class HomeSplash extends React.Component {
  render() {
    const {siteConfig, language = ''} = this.props;
    const {baseUrl, docsUrl} = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

    const Button = props => (
        <div className="cta-button-wrapper">
          <a className="button cta-button" href={props.href} target={props.target}>
            {props.children}
          </a>
        </div>
    );

    return (
      <div className="homeContainer">
          <div className="wrapper homeWrapper">
          <img src={`${baseUrl}img/dv_logo_nobackground.svg`} alt="DV" className="index-hero-img"/>

              <h1 className="tagline">{siteConfig.tagline}</h1>
                <div className="promoRow">
                  <Button href={docUrl('getting-started.html')}>Download</Button>
                  <Button href='http://inivation.com/buy'>Buy a DVS</Button>
                </div>
          </div>
      </div>
    );
  }
}

class Index extends React.Component {
  render() {
    const {config: siteConfig, language = ''} = this.props;
    const {baseUrl} = siteConfig;

    const Block = props => (
      <Container
        padding={['bottom', 'top']}
        id={props.id}
        background={props.background}>
        <GridBlock
          align="center"
          contents={props.children}
          layout={props.layout}
        />
      </Container>
    );

    const BlockOne = () => (
      <Block id="try" background="light">
      {[
        {
          title: 'Results within minutes',
          content: 'DV is ready to use for anyone, even without programming knowledge. Experience the unique power of event based vision with DV.',
          image: `${baseUrl}img/screen.png`,
          imageAlign: 'left',
        },
      ]}
    </Block>
    );




    const BlockTwo = () => (
      <Block >
      {[
        {
          title: 'Bring powerful event based vision to life',
          content: 'DV brings your DVS camera to life. Process data with the many computer vision modules ready-to-use, or develop your own with our SDK.',
          image: `${baseUrl}img/code.png`,
          imageAlign: 'right',
        },
      ]}
    </Block>

    );


    const Features = () => (
      <Block layout="fourColumn">
        {[
          {
            content: 'Visualize and record the output of your DVS sensor',
           // image: `${baseUrl}img/screen.png`,
            imageAlign: 'top',
            title: 'Visualize',
          },
          {
            content: 'Develop your application with the most advanced event based framework in the world',
          //  image: `${baseUrl}img/docusaurus.svg`,
            imageAlign: 'top',
            title: 'Develop',
          },
          {
            content: 'DV helps you deploy your application to devices from IoT to Mainframe',
          //  image: `${baseUrl}img/docusaurus.svg`,
            imageAlign: 'top',
            title: 'Deploy',
          },
          {
            content: 'DV has a trusted backend and an Open Source license',
          //  image: `${baseUrl}img/docusaurus.svg`,
            imageAlign: 'top',
            title: 'Trust',
          },
        ]}
      </Block>
    );


    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer">
          <Features />
          <BlockOne />
          <BlockTwo />
        </div>
      </div>
    );
  }
}

module.exports = Index;
